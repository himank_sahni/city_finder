## Title : City Suggestion

## Description

Get five cities closest to the city that you search.

Planning a trip? Why just go to one city when you can travel to more. With this app it will be easier for you to plan your next vacation

## Technical Summary

- Javascript
- Node.js
- Express
- Mapbox.js
- React.js
- React Bootstrap
- CSS

Using Haversine formala to calculate which 5 cities have the shortest distance to the city searched

# Pictures
![Home](readme_assets/Home.png)
![Search](readme_assets/search.png)
![Map](readme_assets/map.png)

# Run the project

---

- Clone or download the repository
- Open the repo on your machine
- Go to **backend folder**
- In your terminal type `npm i` to install the node packages
- Now go back to main folder and then into **frontend/cities_suggestions** folder and do `npm i` again
- Go back to backend folder again and type `npm run start` in the terminal

***Boom!*** You are nowready for some cities searching

# References

---

- https://en.wikipedia.org/wiki/Haversine_formula
- https://www.movable-type.co.uk/scripts/latlong.html

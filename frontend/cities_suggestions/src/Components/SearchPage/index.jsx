import React from 'react';
import { Container } from 'react-bootstrap';
import './style.css';

import SearchBar from '../Reusables/SearchBar';
import SvgPattern from '../Reusables/SvgPattern';

const SearchPage = () => {
  return (
    <Container className="main-container" fluid>
      <div className="searchPage">
        <h4>City Suggestions</h4>
        <h1> Get cities near any city</h1>
        <div className="search-bar-flex-view">
          <section className="search-bar-view">
            <SearchBar />
          </section>
        </div>
        <SvgPattern />
      </div>
    </Container>
  );
};

export default SearchPage;

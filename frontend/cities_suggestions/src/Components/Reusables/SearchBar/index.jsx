/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-alert */
/* eslint-disable no-console */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from 'react';
import { Button, FormControl } from 'react-bootstrap';
import './style.css';
import { Link, useHistory } from 'react-router-dom';
import InputGroup from 'react-bootstrap/InputGroup';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearchLocation, faSearch } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import {
  getDataFromStorage,
  saveToStorage,
} from '../../../helpers/save_to_local_storage';

const SearchBar = () => {
  const [citiesArr, setcitiesArr] = useState([]);
  const [searchText, setSearchText] = useState('');
  const [hasText, setHasText] = useState(false);
  const history = useHistory();

  /**
   * Filter cities according to what user types in the search bar
   * @param {value} string
   * @param {cities} array
   */
  const filterCities = (value, cities) => {
    const newCitiesArr = cities.filter((city) => {
      const regex = new RegExp(`^${value}`);
      return city.name.toLowerCase().match(regex);
    });
    return newCitiesArr;
  };

  /**
   * Recives cities names either from backend or from local storage
   * @param {value} string
   */
  const getCitiesNames = async (value) => {
    try {
      if (value.length === 1) {
        const { data, status } = await axios.get(
          `http://localhost:4000/city/${value}`
        );
        if (status === 200) {
          setcitiesArr(data);
          saveToStorage(data, 'cities');
        } else {
          throw new Error(`Status is ${status}`);
        }
      }
      const cities = getDataFromStorage('cities');
      setcitiesArr(filterCities(value, cities));
    } catch (error) {
      console.error(error);
    }
  };

  /**
   * Sets the text and hasText value when user types in search bar
   * @param {e} event
   */
  const handleSearchTextChange = async (e) => {
    try {
      setSearchText(e.target.value);
      if (e.target.value.length === 0) {
        setHasText(false);
        setcitiesArr([]);
      } else {
        setHasText(true);
        getCitiesNames(e.target.value.toLowerCase());
      }
    } catch (error) {
      console.error(error);
    }
  };

  /**
   * Look for the cities name on backend when only when letter
   * is typed otherwise search fro it in local memory
   */
  const handleSearch = () => {
    let foundCity = false;
    citiesArr.forEach((city) => {
      if (city.name.toLowerCase() === searchText.toLowerCase()) {
        foundCity = true;
        history.push({
          pathname: `/city/${
            searchText.charAt(0).toUpperCase() +
            searchText.slice(1).toLowerCase()
          }`,
          state: { country: city.country, city: city.name },
        });
      }
    });
    if (!foundCity) {
      alert('Enter a valid availabe city from search');
    }
  };

  return (
    <>
      <InputGroup className="mb-3 search-bar">
        <FormControl
          placeholder="Search cities..."
          aria-label="Search cities..."
          aria-describedby="basic-addon2"
          className="search-input"
          onChange={handleSearchTextChange}
          value={searchText}
        />
        <InputGroup.Append>
          <Button
            variant="light"
            className="search-button"
            onClick={handleSearch}
          >
            <FontAwesomeIcon icon={faSearchLocation} size="1x" />
          </Button>
        </InputGroup.Append>
      </InputGroup>
      <div className={hasText ? 'search-results' : 'no-display'}>
        <ul>
          {citiesArr.map((city) => {
            return (
              <li key={city.id} onClick={() => setSearchText(city.name)}>
                <FontAwesomeIcon icon={faSearchLocation} size="1x" />
                <span className="city-name-text">{city.name}</span>
              </li>
            );
          })}
        </ul>
      </div>
    </>
  );
};

export default SearchBar;

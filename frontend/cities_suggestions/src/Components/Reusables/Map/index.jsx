/* eslint-disable no-console */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/self-closing-comp */
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import ReactMapGl, { FullscreenControl, Marker } from 'react-map-gl';
import { useLocation } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import SearchBar from '../SearchBar';
import './style.css';

require('dotenv').config();

const Map = () => {
  const [viewport, setViewPort] = useState({
    latitute: -70.9,
    longitute: 42.35,
    zoom: 3,
    width: '100vw',
    height: '100vh',
  });
  const location = useLocation();
  const { state } = location;
  const { city, country } = state;

  const [closestCities, setClosestCities] = useState([]);

  /**
   * Recives the selected location coordinate and five cities nearest to
   * the selected coordinate
   */
  useEffect(async () => {
    const { data: coordinates } = await axios.get(
      `http://localhost:4000/city/coordinates/${city}`
    );
    console.log(coordinates);

    setViewPort({
      ...viewport,
      latitude: coordinates.coords[0],
      longitude: coordinates.coords[1],
    });

    const { data } = await axios.get(
      `http://localhost:4000/city/${city}/${country}/coordinates`
    );
    setClosestCities(data.features);
  }, [location]);

  return (
    <div>
      <ReactMapGl
        {...viewport}
        mapStyle="mapbox://styles/mapbox/streets-v11"
        onViewportChange={setViewPort}
        mapboxApiAccessToken={process.env.REACT_APP_MAP_BOX_API_KEY}
      >
        <FullscreenControl />
        <SearchBar className="search-bar-view-map" />
        {closestCities.length > 1
          ? closestCities.map((closestCity) => (
              <Marker
                key={`${closestCity.geometry.coordinates[0]} + ${closestCity.geometry.coordinates[1]}`}
                longitude={closestCity.geometry.coordinates[0]}
                latitude={closestCity.geometry.coordinates[1]}
              >
                <FontAwesomeIcon icon={faMapMarkerAlt} size="2x" />
              </Marker>
            ))
          : null}
      </ReactMapGl>
    </div>
  );
};

export default Map;

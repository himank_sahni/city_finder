import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Map from './Components/Reusables/Map';
import SearchPage from './Components/SearchPage';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={SearchPage} />
        <Route path="/city/" component={Map} />
      </Switch>
    </Router>
  );
}

export default App;

export const saveToStorage = (data, name) => {
  sessionStorage.setItem(name, JSON.stringify(data));
};

export const getDataFromStorage = (name) => {
  const cities = sessionStorage.getItem(name);
  return JSON.parse(cities);
};

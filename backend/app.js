const express = require('express');
const cors = require('cors');
const router = require('./routers');

const PORT = process.env.PORT || 4000;
const app = express();

app.use(express.json());
app.use(cors());
app.use('/city', router)

app.listen(PORT, () => {
	console.log(`Server running on Port ${PORT}`);
});

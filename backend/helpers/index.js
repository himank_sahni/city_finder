const convertToRadians = (numInDeg) => {
	return numInDeg * (Math.PI / 180);
};
const haversineFunct = (theta) => {
	return Math.sin(theta / 2) ** 2;
};
module.exports.haverSinesDistance = (origin, dest) => {
	const earthRadius = 6371;
	const [latO, lonO] = origin;
	const [latD, lonD] = dest;

	const [latORad, lonORad] = [convertToRadians(latO), convertToRadians(lonO)];
	const [latDRad, lonDRad] = [convertToRadians(latD), convertToRadians(lonD)];

	const hav =
		haversineFunct(latDRad - latORad) +
		Math.cos(latORad) * Math.cos(latDRad) * haversineFunct(lonDRad - lonORad);
	const c = 2 * Math.atan2(Math.sqrt(hav), Math.sqrt(1 - hav));
	const dist = earthRadius * c;

	return dist;
};

module.exports.getCoordinates = (city, cities) => {
	for (const city_loop in cities) {
		if (cities[city_loop].name.toLowerCase() === city.toLowerCase()) {
			return [cities[city_loop].location.lat, cities[city_loop].location.lon];
		}
	}
};

module.exports.addCoordinatnes = (citiesWithSameCountry, data, city_loop, dist) => {
	citiesWithSameCountry[`${dist}`] = {
		type: 'Feature',
		properties: {},
		geometry: {
			type: 'Point',
			coordinates: [data[city_loop].location.lon, data[city_loop].location.lat],
		},
	};
};

module.exports.covertToFeatures = (obj) => {
	const features = [];
	for (const dist in obj) {
		features.push(obj[dist]);
	}
	return features;
};

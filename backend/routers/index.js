const { Router } = require('express');
const { getSearchSelection, getClosestCitiesCoord, getCityCoord } = require('../controllers');

const router = Router();
router.route('/:name').get(getSearchSelection);
router.route('/:cityName/:country/coordinates').get(getClosestCitiesCoord);
router.route('/coordinates/:cityName').get(getCityCoord);

module.exports = router;

const data = require('../data/cities.json');
const helpers = require('../helpers');
const { MaxHeap } = require('@datastructures-js/max-heap');

module.exports.getSearchSelection = async (req, res) => {
	try {
		const cityText = req.params.name;
		const citiesNames = [];
		for (const city in data) {
			if (data[city].name.charAt(0).toLowerCase() === cityText) {
				citiesNames.push({
					name: data[city].name,
					country: data[city].countryName,
					id: data[city].id,
				});
			}
		}
		res.send(citiesNames);
	} catch (error) {
		res.status(500).send({ error: { name: error.name, message: error.message } });
	}
};

module.exports.getClosestCitiesCoord = (req, res) => {
	try {
		const { cityName, country } = req.params;
		const citiesWithSameCountry = {};
		const citiesWithMinDistToOrigin = new MaxHeap();
		const orignCoord = helpers.getCoordinates(cityName, data);

		for (const city_loop in data) {
			if (data[city_loop].name !== cityName && data[city_loop].countryName === country) {
				const destCoord = [data[city_loop].location.lat, data[city_loop].location.lon];
				const dist = helpers.haverSinesDistance(orignCoord, destCoord);

				if (citiesWithMinDistToOrigin.size() === 5) {
					if (citiesWithMinDistToOrigin.root() > dist) {
						delete citiesWithSameCountry[citiesWithMinDistToOrigin.root()];
						citiesWithMinDistToOrigin._nodes[0] = dist;
						citiesWithMinDistToOrigin.fix();
						helpers.addCoordinatnes(citiesWithSameCountry, data, city_loop, dist);
					}
				} else if (citiesWithMinDistToOrigin.size() < 5) {
					citiesWithMinDistToOrigin.insert(dist);
					citiesWithMinDistToOrigin.fix();
					helpers.addCoordinatnes(citiesWithSameCountry, data, city_loop, dist);
				}
			}
		}

		res.send({ type: 'FeatureCollection', features: helpers.covertToFeatures(citiesWithSameCountry) });
	} catch (error) {
		res.status(500).send({ error: { name: error.name, message: error.message } });
	}
};

module.exports.getCityCoord = (req, res) => {
	const { cityName } = req.params;
	const coords = helpers.getCoordinates(cityName, data);
	res.send({ coords });
};
